<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use League\Csv\Writer;

final class DataController
{
    private function getTableColumns()
    {
        $columns = DB::select("
            SELECT
                COLUMN_NAME, DATA_TYPE
            FROM
                information_schema.COLUMNS
            WHERE
                TABLE_SCHEMA = 'suridata'
                AND TABLE_NAME = 'dados'
            ");

        return array_map(function (\stdClass $column) {
            switch ($column->DATA_TYPE) {
                case 'int':
                case 'decimal':
                    $type = 'number';
                    break;
                case 'date':
                    $type = 'date';
                    break;
                default:
                    $type = 'text';
            }

            return [
                'name' => $column->COLUMN_NAME,
                'type' => $type,
            ];
        }, $columns);
    }

    public function fetch()
    {
        return response()->json([
            'columns' => $this->getTableColumns(),
            'data' => DB::table('dados')->get(),
        ]);
    }

    public function download()
    {
        $data = DB::table('dados')->get();
        $headers = array_keys((array) $data[0]);

        $csv = Writer::createFromFileObject(new \SplTempFileObject);
        $csv->insertOne($headers);

        foreach ($data as $row) {
            $csv->insertOne((array) $row);
        }

        return response((string) $csv, 200, [
            'Content-Type' => 'text/csv',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="suridata.csv"',
        ]);
    }
}
