const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
const isPDF = process.argv.reduce((a, b) => {
  if (b === '--pdf') {
    return true;
  }

  return a;
}, false);

if (isPDF) {
  mix.sass('resources/scss/pdf.scss', 'public/css/pdf.css');
}

mix.react('resources/js/index.js', 'public/js/app.js')
    .sass('resources/scss/index.scss', 'public/css/app.css', [
        //
    ]);
