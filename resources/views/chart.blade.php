<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gerador de grafico</title>
        <link href="{{ $assets }}css/pdf.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
    <div class="topbar">
        <img src="{{ $assets }}images/suridata.png" />
    </div>
    <div class="content">
        <div class="wrapper">
            @if ($image)
                <div class="image" style="background-image: url('{{ $assets . $image }}')"></div>
            @else
                <h1 class="text">
                    Grafico nao encontrado
                </h1>
            @endif
        </div>
    </div>
    <div class="footer">
        <p>Suridata &copy; all rights reserved</p>
    </div>
    </body>
</html>
