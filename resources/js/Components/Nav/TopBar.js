import React from 'react';
import { Navbar } from 'react-bootstrap';

export default function TopBar() {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">
          Suridata
        </Navbar.Brand>
      </Navbar>
    </>
  );
}
