import React from "react";
import { Button, Col, Row } from "react-bootstrap";

import WidgetForm from "./Widget/Form";
import FilterForm from "./Filter/Form";

export default function Forms({ data, columns, filters, onAddWidget, onAddFilter, onRemoveFilter }) {
  return (
    <Row>
      <Col xs={10}>
        <WidgetForm
          data={data}
          columns={columns}
          onAdd={onAddWidget}
        />
        <FilterForm
          data={data}
          columns={columns}
          onAdd={onAddFilter}
          onRemove={onRemoveFilter}
          filters={filters}
        />
      </Col>
      <Col xs={2}>
        <Button variant="primary" href="/download" download>Baixar dados do banco</Button>
      </Col>
    </Row>
  );
}
