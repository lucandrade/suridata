import React from "react";
import { Col, Form, Row } from "react-bootstrap";

export default function Input({ label, children, labelColumns = 3 }) {
  return (
    <Form.Group as={Row}>
      <Form.Label column xs={labelColumns}>{label}</Form.Label>
      <Col xs={12 - labelColumns}>{children}</Col>
    </Form.Group>
  );
}
