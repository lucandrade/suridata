import React, { useState } from "react";
import { Button } from "react-bootstrap";

import Badge from "./Badge";
import ColorPicker from "../../Widget/ColorPicker";

export default function Colors({ colors, onChange }) {
  const [isOpen, setIsOpen] = useState(false);
  const [color, setColor] = useState('#fff');

  const handleAdd = (color) => {
    setColor(color.hex);

    if (color.source !== 'hsv') {
      return;
    }

    onChange([...colors, color.hex]);
    setIsOpen(false);
  };

  return (
    <>
      <Button
        variant={"light"}
        size={"sm"}
        onClick={() => setIsOpen(true)}
        className="mr-2">Selecionar</Button>
      <ColorPicker
        color={color}
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        onChange={handleAdd}
      />
      {colors.map((c, i) => (
        <Badge
          key={c}
          backgroundColor={c}
          text={`Cor #${i + 1}`}
          onClick={() => onChange([...colors.filter(color => color !== c)])} />
      ))}
    </>
  );
}
