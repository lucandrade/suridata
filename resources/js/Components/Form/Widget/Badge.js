import React from "react";

export default function Badge({ text, onClick, backgroundColor }) {
  const classes = "badge badge-dark p-2 text-white rounded-pill mr-2 pointer";
  const style = {};

  if (backgroundColor) {
    style.backgroundColor = backgroundColor;
  }

  return (
    <span className={classes} style={style} onClick={onClick}>
      {text}
    </span>
  )
}
