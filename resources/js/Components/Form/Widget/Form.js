import React, { useState } from 'react';
import {
  Button,
  ButtonGroup,
  Modal,
} from 'react-bootstrap';

import BasicChartForm from "./BasicChartForm";
import Input from "../Input";
import TableForm from "./TableForm";
import { WIDGET_TYPES } from "../../../Connstants";

export default function Form({ data, columns, onAdd }) {
  const [isOpen, setIsOpen] = useState(false);
  const [type, setType] = useState(WIDGET_TYPES[0]);

  const handleOpen = () => setIsOpen(true);
  const handleClose = () => setIsOpen(false);
  const handleSubmit = (data) => {
    onAdd({ type, form: data });
    setIsOpen(false);
    setType(WIDGET_TYPES[0]);
  };

  return (
    <>
      <Button variant="primary" onClick={handleOpen}>Adicionar grafico</Button>
      <Modal
        show={isOpen}
        onHide={handleClose}
        keyboard={false}
        centered
        backdrop="static">
        <Modal.Header closeButton>Adicionar grafico</Modal.Header>
        <Modal.Body>
          <Input label={"Tipos"}>
            <ButtonGroup size="sm">
              {WIDGET_TYPES.map(t => (
                <Button
                  key={t}
                  variant={t === type ? 'primary' : 'secondary'}
                  onClick={() => setType(t)}>{t}</Button>
              ))}
            </ButtonGroup>
          </Input>
          {type !== 'Tabela' && (
            <BasicChartForm
              data={data}
              columns={columns}
              onSubmit={handleSubmit} />
          )}
          {type === 'Tabela' && (
            <TableForm
              data={data}
              columns={columns}
              onSubmit={handleSubmit} />
          )}
        </Modal.Body>
      </Modal>
    </>
  );
}
