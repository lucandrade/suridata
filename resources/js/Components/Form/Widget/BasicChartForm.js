import React, { useEffect, useState } from 'react';
import {
  Button,
  Form,
} from 'react-bootstrap';

import { selectDimensionOptions, selectMetricColumns } from "../../../Selectors/FormSelector";
import Colors from "./Colors";
import Input from "../Input";
import Badge from "./Badge";
import { dataValue, titleCase } from "../../../Functions";

export default function BasicChartForm({ data, columns, onSubmit }) {
  const [name, setName] = useState('');
  const [metric, setMetric] = useState('');
  const [dimension, setDimension] = useState('');
  const [dimensions, setDimensions] = useState([]);
  const [colors, setColors] = useState([]);
  const [dimensionOptions, setDimensionOptions] = useState([]);

  useEffect(() => {
    setDimensionOptions(dimension ? selectDimensionOptions(data, dimension) : []);
    setDimensions([]);
  }, [dimension]);

  const handleSubmit = (e) => {
    if (name && metric && dimension) {
      onSubmit({ name, metric, dimension, colors, dimensions });
    }

    e.preventDefault();
    return false;
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Input label={"Nome"}>
        <Form.Control
          type="text"
          placeholder={"Nome"}
          value={name}
          onChange={e => setName(e.target.value)}
        />
      </Input>
      <Input label={"Dimensao"}>
        <Form.Control
          className="text-capitalize"
          as="select"
          custom
          value={dimension}
          onChange={e => setDimension(e.target.value)}>
          <option value={''}>Selecione</option>
          {columns.map(c => (
            <option key={c.name} value={c.name}>{titleCase(c.name)}</option>
          ))}
        </Form.Control>
      </Input>
      <Input label={"Metrica"}>
        <Form.Control
          className="text-capitalize"
          as="select"
          custom
          value={metric}
          onChange={e => setMetric(e.target.value)}>
          <option value={''}>Selecione</option>
          {selectMetricColumns(columns).map(c => (
            <option key={c.name} value={c.name}>{titleCase(c.name)}</option>
          ))}
        </Form.Control>
      </Input>
      {dimensionOptions.length > 0 && (
        <Input label={"Dimensoes"}>
          <Form.Control
            className="text-capitalize"
            as="select"
            custom
            value={''}
            onChange={e => setDimensions([...dimensions, e.target.value])}>
            <option value={''}>Selecione</option>
            {dimensionOptions.filter(o => !dimensions.includes(o.toString())).map(c => (
              <option key={c} value={c}>{dataValue(c)}</option>
            ))}
          </Form.Control>
          {dimensions.length > 0 && (
            <div className="mt-3">
              {dimensions.map((d, i) => (
                <Badge
                  key={i}
                  text={dataValue(d)}
                  onClick={() => setDimensions([...dimensions.filter(t => t !== d)])}
                />
              ))}
            </div>
          )}
        </Input>
      )}
      <Input label={"Cores"}>
        <Colors colors={colors} onChange={setColors} />
      </Input>
      <Button
        variant="primary"
        type="submit"
        disabled={!name || !metric || !dimension}>Salvar</Button>
    </Form>
  );
}
