import React, { useEffect, useState } from 'react';
import {
  Button,
  Form as BootstrapForm,
  Modal,
} from 'react-bootstrap';

import { titleCase } from "../../../Functions";
import Input from "../Input";
import { FILTER_OPTION_EQUAl } from "../../../Connstants";
import Filter from "./Filter";

export default function Form({ columns, filters, onAdd, onRemove }) {
  const [column, setColumn] = useState('');
  const [value, setValue] = useState('');
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (!isOpen) {
      setColumn('');
    }
  }, [isOpen]);

  useEffect(() => {
    setValue('');
  }, [column]);

  const handleOpen = () => setIsOpen(true);
  const handleClose = () => setIsOpen(false);

  const handleSubmit = e => {
    e.preventDefault();
    onAdd(column, FILTER_OPTION_EQUAl, value);
    setIsOpen(false);
    return false;
  };

  return (
    <>
      <Button variant="primary" className="ml-2" onClick={handleOpen}>Adicionar Filtro</Button>
      {filters.map((f, i) => (
        <Filter key={i} {...f} onClick={() => onRemove(f)} />
      ))}
      <Modal
        show={isOpen}
        onHide={handleClose}
        keyboard={false}
        centered
        backdrop="static">
        <Modal.Header closeButton>Adicionar Filtro</Modal.Header>
        <Modal.Body>
          <BootstrapForm onSubmit={handleSubmit}>
            <Input label={"Coluna"}>
              <BootstrapForm.Control
                className="text-capitalize"
                as="select"
                custom
                value={column}
                onChange={e => setColumn(e.target.value)}>
                <option value={''}>Selecione</option>
                {columns.map(c => (
                  <option key={c.name} value={c.name}>{titleCase(c.name)}</option>
                ))}
              </BootstrapForm.Control>
            </Input>
            {column && (
              <Input label={"Valor"}>
                <BootstrapForm.Control
                  value={value}
                  onChange={e => setValue(e.target.value)} />
              </Input>
            )}
            <Button
              variant="primary"
              type="submit"
              disabled={!value || !column}>Salvar</Button>
          </BootstrapForm>
        </Modal.Body>
      </Modal>
    </>
  );
}
