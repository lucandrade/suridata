import React from "react";
import { Button } from "react-bootstrap";

import { titleCase } from "../../../Functions";
import { FILTER_OPTION_GREATER_THAN, FILTER_OPTION_LESS_THAN } from "../../../Connstants";

export default function Filter({ column, operation, value, onClick }) {
  let operationText = 'Igual a';

  if (operation === FILTER_OPTION_LESS_THAN) {
    operationText = 'Abaixo de';
  } else if (operation === FILTER_OPTION_GREATER_THAN) {
    operationText = 'Acime de';
  }

  return (
    <Button variant="outline-dark" size="sm" className="ml-2" onClick={onClick}>
      <span className="text-capitalize">{`${titleCase(column)} `}</span>
      <span>{`${operationText} `}</span>
      <strong>{value}</strong>
    </Button>
  );
}
