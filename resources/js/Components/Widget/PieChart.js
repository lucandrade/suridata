import React, { Component } from "react";
import Highcharts from 'highcharts';
import { v4 } from 'uuid';

import { generateColors, titleCase } from "../../Functions";

const CHART_OPTS = {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie',
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
      },
      showInLegend: true,
    },
  },
};

export default class PieChart extends Component {
  constructor(props) {
    super(props);
    this.chartRef = null;
    this.chartId = v4();
  }

  componentDidMount() {
    this.updateChart();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (JSON.stringify(prevProps.datasets) !== JSON.stringify(this.props.datasets)) {
      this.updateChart();
    }

    if (JSON.stringify(prevProps.labels) !== JSON.stringify(this.props.labels)) {
      this.updateChart();
    }

    if (JSON.stringify(prevProps.colors) !== JSON.stringify(this.props.colors)) {
      this.updateChart();
    }
  }

  updateChart() {
    const { datasets, metric, dimension, labels, colors, onClick } = this.props;

    const generatedColors = generateColors(colors, labels.length);

    const chartData = {
      ...CHART_OPTS,
      colors: generatedColors,
      plotOptions: {
        ...CHART_OPTS.plotOptions,
        pie: {
          ...CHART_OPTS.plotOptions.pie,
          events: {
            click: (e) => {
              onClick(labels[e.point.index]);
              e.preventDefault();
            },
          },
        },
      },
      title: {
        text: `${titleCase(dimension)} por ${titleCase(metric)}`,
        style: {
          textTransform: 'capitalize',
        },
      },
      series: [{
        name: 'Brands',
        colorByPoint: true,
        data: datasets.map((d, i) => ({
          name: labels[i],
          y: d,
        }))
      }],
    };

    if (this.chartRef) {
      this.chartRef.destroy();
    }

    this.chartRef = Highcharts.chart(this.chartId, chartData);
  }

  render() {
    return (
      <div id={this.chartId} />
    );
  }
}
