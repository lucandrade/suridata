import React, { Component } from "react";
import Highcharts from 'highcharts';
import { v4 } from 'uuid';

import { generateColors, titleCase } from "../../Functions";

const CHART_OPTS = {
  chart: {
    type: 'column',
  },
  xAxis: {
    categories: [''],
  },
  plotOptions: {
    column: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
      },
    },
    series: {
      states: {
        hover: {
          enabled: false,
        },
      },
    },
  },
};

export default class BarChart extends Component {
  constructor(props) {
    super(props);
    this.chartRef = null;
    this.chartId = v4();
  }

  componentDidMount() {
    this.updateChart();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (JSON.stringify(prevProps.datasets) !== JSON.stringify(this.props.datasets)) {
      this.updateChart();
    }

    if (JSON.stringify(prevProps.labels) !== JSON.stringify(this.props.labels)) {
      this.updateChart();
    }

    if (JSON.stringify(prevProps.colors) !== JSON.stringify(this.props.colors)) {
      this.updateChart();
    }
  }

  updateChart() {
    const { datasets, metric, dimension, labels, colors, onClick } = this.props;

    const generatedColors = generateColors(colors, labels.length);

    const chartData = {
      ...CHART_OPTS,
      colors: generatedColors,
      plotOptions: {
        ...CHART_OPTS.plotOptions,
        column: {
          ...CHART_OPTS.plotOptions.column,
          events: {
            click: (e) => {
              onClick(labels[e.point.series.columnIndex]);
              e.preventDefault();
            },
          },
        },
      },
      title: {
        text: `${titleCase(dimension)} por ${titleCase(metric)}`,
        style: {
          textTransform: 'capitalize',
        },
      },
      series: datasets.map((d, i) => ({
        name: labels[i],
        data: [d],
      })),
    };

    if (this.chartRef) {
      this.chartRef.destroy();
    }

    this.chartRef = Highcharts.chart(this.chartId, chartData);
  }

  render() {
    return (
      <div id={this.chartId} />
    );
  }
}
