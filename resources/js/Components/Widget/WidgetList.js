import React from "react";
import { Col, Row } from "react-bootstrap";

import Widget from "./Widget";

export default function WidgetList({ widgets, data, onOpen, onRemove }) {
  return (
    <Row>
      {widgets.map((widget, i) => (
        <Col xs={4} key={i} className="mt-4">
          <Widget data={data} {...widget} onClick={onOpen} onRemove={() => onRemove(i)} />
        </Col>
      ))}
    </Row>
  );
}
