import React from "react";
import { Card } from "react-bootstrap";

import { transformChartData } from "../../Functions";
import Table from "./Table";
import PieChart from "./PieChart";
import BarChart from "./BarChart";

export default function Widget({ form, type, data, onClick, onRemove }) {
  const { name, dimension, metric, colors, dimensions } = form;
  const { datasets, labels } = transformChartData(data, dimension, metric, dimensions);

  const handleClick = label => {
    onClick(dimension, label);
  };

  return (
    <Card>
      <Card.Body>
        <Card.Title className="text-center">
          {name}
          <span className="float-right pointer" onClick={onRemove}>&times;</span>
        </Card.Title>
        <div>
          {type === 'Barra' && (
            <BarChart
              type={'bar'}
              colors={colors}
              datasets={datasets}
              labels={labels}
              metric={metric}
              dimension={dimension}
              onClick={handleClick}
            />
          )}
          {type === 'Pizza' && (
            <PieChart
              colors={colors}
              datasets={datasets}
              labels={labels}
              metric={metric}
              dimension={dimension}
              onClick={handleClick}
            />
          )}
          {type === 'Tabela' && (
            <Table
              datasets={datasets}
              labels={labels}
              metric={metric}
              dimension={dimension}
              onClick={handleClick}
            />
          )}
        </div>
      </Card.Body>
    </Card>
  );
}
