import React from "react";
import { ChromePicker } from "react-color";

export default function ColorPicker({ color, isOpen, onChange, onClose }) {
  const popover = {
    position: 'absolute',
    zIndex: '2',
  };

  const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
  };

  if (!isOpen) {
    return null;
  }

  return (
    <div style={popover}>
      <div style={cover} onClick={onClose} />
      <ChromePicker disableAlpha={true} color={color} onChangeComplete={onChange} />
    </div>
  );
}