import React from "react";
import { Table as BootstrapTable } from "react-bootstrap";
import { formatChartValue, titleCase } from "../../Functions";

export default function Table({ dimension, metric, datasets, labels }) {
  return (
    <BootstrapTable striped bordered size="sm">
      <thead>
        <tr className="text-capitalize">
          <th>{titleCase(dimension)}</th>
          <th>{titleCase(metric)}</th>
        </tr>
      </thead>
      <tbody>
        {datasets.map((value, i) => (
          <tr key={i}>
            <td>{labels[i]}</td>
            <td>{formatChartValue(metric, value)}</td>
          </tr>
        ))}
      </tbody>
    </BootstrapTable>
  );
}
