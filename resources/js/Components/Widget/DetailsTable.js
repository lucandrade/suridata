import React from "react";
import { Button, Modal, Table } from "react-bootstrap";

import { dataValue, titleCase } from "../../Functions";
import { filterData } from "../../Selectors/WidgetSelector";
import { EMPTY_TEXT } from "../../Connstants";

export default function DetailsTable({ data, columns, dimension, value, onClose }) {
  let filteredData = [];

  if (dimension && value) {
    const filter = {};
    filter[dimension] = [value === EMPTY_TEXT ? '' : value];

    filteredData = filterData(data, filter);
  }

  return (
    <Modal
      dialogClassName="mw-100 mx-5 modal-dialog-scrollable"
      contentClassName="overflow-auto"
      centered
      show={dimension !== null}
      onHide={onClose}
    >
      <Modal.Header className="text-capitalize" closeButton>
        {titleCase(dimension)}: {value}
      </Modal.Header>
      <Modal.Body>
        <Table striped bordered size="sm" style={{ fontSize: "85%" }}>
          <thead>
            <tr className="text-capitalize">
              <th>{titleCase(dimension)}</th>
              {columns.filter(c => c.name !== dimension).map(c => (
                <th key={c.name}>{titleCase(c.name)}</th>
              ))}
            </tr>
          </thead>
          <tbody>
          {filteredData.map((row, i) => (
            <tr key={i}>
              <td>{dataValue(row[dimension])}</td>
              {columns.filter(c => c.name !== dimension).map(c => (
                <td key={c.name}>{dataValue(row[c.name])}</td>
              ))}
            </tr>
          ))}
          </tbody>
        </Table>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={onClose}>Fechar</Button>
      </Modal.Footer>
    </Modal>
  );
}
