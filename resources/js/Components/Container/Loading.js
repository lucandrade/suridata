import React from "react";

import TopBar from "../Nav/TopBar";
import Content from "./Content";

export default function Loading() {
  return (
    <>
      <TopBar />
      <Content>
        Loading
      </Content>
    </>
  );
}
