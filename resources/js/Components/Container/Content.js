import React from "react";
import { Col, Container, Row } from "react-bootstrap";

export default function Content({ children }) {
  return (
    <Container fluid className="mt-5">
      <Row>
        <Col>
          {children}
        </Col>
      </Row>
    </Container>
  );
}
