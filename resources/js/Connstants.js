export const EMPTY_TEXT = 'Nao Informado';
export const WIDGET_TYPES = ['Barra', 'Pizza', 'Tabela'];
export const CURRENCY_COLUMNS = ['valor_prestador_grupo', 'valor_ps'];
export const FILTER_OPTION_GREATER_THAN = 0;
export const FILTER_OPTION_LESS_THAN = 1;
export const FILTER_OPTION_EQUAl = 2;
