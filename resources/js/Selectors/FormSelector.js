export const selectMetricColumns = (columns) => {
  return columns.filter(c => c.type === 'number' && c.name !== 'id');
};

export const selectDimensionOptions = (data, dimension) => {
  return data.reduce((carry, item) => {
    if (carry.indexOf(item[dimension]) < 0) {
      carry.push(item[dimension]);
    }

    return carry;
  }, []);
};
