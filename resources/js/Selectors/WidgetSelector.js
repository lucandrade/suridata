import {
  FILTER_OPTION_EQUAl,
  FILTER_OPTION_GREATER_THAN,
  FILTER_OPTION_LESS_THAN,
} from "../Connstants";

const isValid = (filters, value) => {
  for (const filter of filters) {
    if (typeof filter === 'string') {
      if (filter === value) {
        return true;
      }

      continue;
    }

    if (filter[0] === FILTER_OPTION_EQUAl && filter[1] === value) {
      return true;
    }

    if (filter[0] === FILTER_OPTION_GREATER_THAN && filter[1] > value) {
      return true;
    }

    if (filter[0] === FILTER_OPTION_LESS_THAN && filter[1] < value) {
      return true;
    }
  }

  return false;
};

export const filterData = (data, filters)  => {
  if (Object.keys(filters).length < 1) {
    return data;
  }

  return Object.keys(filters).reduce((carry, key) => {
    return carry.filter(i => isValid(filters[key], i[key]));
  }, data);
};

export const formatFilters = filters => {
  return filters.reduce((carry, filter) => {
    if (!carry.hasOwnProperty(filter.column)) {
      carry[filter.column] = [];
    }

    carry[filter.column].push([filter.operation, filter.value]);

    return carry;
  }, {});
};
