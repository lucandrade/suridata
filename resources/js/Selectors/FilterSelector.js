import { FILTER_OPTION_EQUAl, FILTER_OPTION_GREATER_THAN, FILTER_OPTION_LESS_THAN } from "../Connstants";

export const selectOptions = () => [
  {
    label: 'Igual',
    value: FILTER_OPTION_EQUAl,
  },
  {
    label: 'Maior',
    value: FILTER_OPTION_GREATER_THAN,
  },
  {
    label: 'Menor',
    value: FILTER_OPTION_LESS_THAN,
  },
];