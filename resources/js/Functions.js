import Axios from 'axios';
import Colors from 'randomcolor';

import { filterData } from "./Selectors/WidgetSelector";
import { CURRENCY_COLUMNS, EMPTY_TEXT } from "./Connstants";

export const loadData = async () => {
  try {
    const { data } = await Axios.get('/api/data');
    return data;
  } catch (e) {
    return false;
  }
};

export const formatChartValue = (metric, value) => {
  if (CURRENCY_COLUMNS.includes(metric)) {
    return `\$${new Intl.NumberFormat().format(value)}`;
  }

  return value;
};

export const transformChartData = (data, dimension, metric, dimensions = []) => {
  const finalFilters = dimensions.length > 0 ? { [dimension]: dimensions } : {};

  const transformed = filterData(data, finalFilters).reduce((carry, row) => {
    if (!carry.hasOwnProperty(row[dimension])) {
      carry[row[dimension]] = 0;
    }

    if (!row[metric]) {
      return carry;
    }

    const value = row[metric].toString().indexOf('.') >= 0 ?
      Number.parseFloat(row[metric]) :
      Number.parseInt(row[metric]);

    carry[row[dimension]] += value;

    return carry;
  }, {});

  return {
    datasets: Object.keys(transformed).map(c => transformed[c]),
    labels: Object.keys(transformed).map(dataValue),
  };
};

export const generateColors = (colors, amount) => {
  if (colors.length >= amount) {
    return colors;
  }

  const randomColors = Colors({
    count: amount,
    hue: 'random',
  });

  return [...colors, ...randomColors];
};

export const dataValue = text => text || EMPTY_TEXT;
export const titleCase = text => dataValue(text).split('_').join(' ');
