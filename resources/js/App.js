import React, { useEffect, useState }  from 'react';

import TopBar from './Components/Nav/TopBar';
import { loadData } from "./Functions";
import Loading from "./Components/Container/Loading";
import Content from "./Components/Container/Content";
import WidgetList from "./Components/Widget/WidgetList";
import DetailsTable from "./Components/Widget/DetailsTable";
import Forms from "./Components/Form/Forms";
import { filterData, formatFilters } from "./Selectors/WidgetSelector";

export default function App() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [dbData, setDBData] = useState(null);
  const [widgets, setWidgets] = useState([{
    type: 'Barra',
    form: {
      name: 'Grafico',
      metric: 'valor_ps',
      dimension: 'tipo_internacao',
      colors: [],
      dimensions: [],
    }
  }]);
  const [filters, setFilters] = useState([]);
  const [openedDimension, setOpenedDimension] = useState(null);
  const [openedDimensionValue, setOpenedDimensionValue] = useState(null);

  useEffect(() => {
    const timerID = setTimeout(async () => {
      const data = await loadData();
      setDBData(data);
    }, 1000);

    return () => clearTimeout(timerID);
  }, []);

  useEffect(() => {
    setIsLoaded(dbData !== null);
  }, [dbData]);

  if (!isLoaded) {
    return <Loading />;
  }

  const handleAddWidget = data => setWidgets([...widgets, data]);
  const handleRemoveWidget = index => {
    widgets.splice(index, 1);
    setWidgets([...widgets]);
  };
  const handleAddFilter = (column, operation, value) => setFilters([...filters, {
    column, operation, value
  }]);
  const handleRemoveFilter = ({ column, operation, value }) => setFilters([
    ...filters.filter(f => f.column !== column && f.operation !== operation && f.value !== value)
  ]);
  const handleOpenDetails = (dimension, value) => {
    setOpenedDimension(dimension);
    setOpenedDimensionValue(value);
  };
  const handleCloseDetails = () => handleOpenDetails(null, null);
  const filteredData = filterData(dbData.data, formatFilters(filters));

  return (
    <>
      <TopBar />
      <Content>
        <Forms
          data={filteredData}
          columns={dbData.columns}
          filters={filters}
          onAddWidget={handleAddWidget}
          onAddFilter={handleAddFilter}
          onRemoveFilter={handleRemoveFilter}
        />
        <WidgetList
          widgets={widgets}
          data={filteredData}
          onOpen={handleOpenDetails}
          onRemove={handleRemoveWidget}
        />
        <DetailsTable
          data={filteredData}
          columns={dbData.columns}
          dimension={openedDimension}
          value={openedDimensionValue}
          onClose={handleCloseDetails}
        />
      </Content>
    </>
  );
}
